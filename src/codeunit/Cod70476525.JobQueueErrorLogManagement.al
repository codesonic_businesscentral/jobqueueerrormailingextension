codeunit 70476525 "JobQueueErrorLogManagement"
{
    [EventSubscriber(ObjectType::Codeunit, 448, 'OnAfterExecuteJob', '', true, true)]
    LOCAL PROCEDURE OnAfterExecutionErrorJobQueueEntry(var JobQueueEntry: Record "Job Queue Entry"; WasSuccess: Boolean);
    VAR
        JQEmailSetup: Record "Job Queue E-Mail Setup";
        JQMailingGroup: Record "JQ Mailing Group";
        ToEmails: list of [Text];
        CCEmails: List of [Text];
        SubjTxt: Label '%1 %2: Fehler';
    BEGIN
        if WasSuccess then
            exit;
        IF GUIALLOWED THEN
            EXIT;
        IF NOT JQEmailSetup.GET THEN
            EXIT;
        IF (JQEmailSetup."Send Mail From Address" = '') OR (JQEmailSetup."Send Mail From Name" = '') THEN
            EXIT;

        JQMailingGroup.SETRANGE(Inactive, FALSE);
        IF JQMailingGroup.ISEMPTY THEN
            EXIT;

        JQMailingGroup.FINDSET(FALSE, FALSE);
        REPEAT
            JoinEmailsByJQGroup(JQMailingGroup.Code, ToEmails);
            IF ToEmails.Count <> 0 THEN
                WITH JobQueueEntry DO
                    LogMailingStatus(
                      SendMassMailTo(
                        ToEmails, CCEmails,
                        ParseEmailSubject(JobQueueEntry),
                        ParseEmailBody(JobQueueEntry)));
        UNTIL JQMailingGroup.NEXT = 0;
    END;

    LOCAL PROCEDURE JoinEmailsByJQGroup(JQMailGroupCode: Code[10]; var AllMails: list of [Text])
    VAR
        JQMailingGroupMember: Record "JQ Mailing Group Member";
    BEGIN
        JQMailingGroupMember.SETRANGE("JQ Mailing Group Code", JQMailGroupCode);
        JQMailingGroupMember.SETFILTER("E-Mail", '<>%1', '');
        IF JQMailingGroupMember.FINDSET(FALSE, FALSE) THEN
            REPEAT
                AllMails.Add(JQMailingGroupMember."E-Mail");
            UNTIL JQMailingGroupMember.NEXT = 0;
    END;

    LOCAL PROCEDURE SendMassMailTo(EmailListTo: list of [Text]; EmailListCC: list of [Text]; Subject: Text; Body: Text): Text;
    VAR
        JQMailSetup: Record "Job Queue E-Mail Setup";
        SMTPMail: Codeunit "SMTP Mail";
    BEGIN
        JQMailSetup.GET; // unconditional: already checked in OnAfterExecutionErrorJobQueueEntry()
        CLEAR(SMTPMail);

        SMTPMail.CreateMessage(
          JQMailSetup."Send Mail From Name", JQMailSetup."Send Mail From Address",
          EmailListTo,
          Subject,
          Body);

        IF EmailListCC.Count > 0 THEN
            SMTPMail.AddCC(EmailListCC);

        IF NOT SMTPMail.Send() THEN
            EXIT(SMTPMail.GetLastSendMailErrorText());
    END;

    LOCAL PROCEDURE LogMailingStatus(MessageIn: Text);
    BEGIN
        IF MessageIn = '' THEN
            EXIT;
        // otherwise do smth please
    END;

    LOCAL PROCEDURE ParseEmailSubject(VAR JobQueueEntry: Record "Job Queue Entry") EmailSubject: Text;
    VAR
        ErrorOccurredTxt: Label 'ein Fehler ist aufgetreten';
    BEGIN
        EmailSubject := STRSUBSTNO('%1: %2 (%3)', JobQueueEntry.Description, ErrorOccurredTxt, CURRENTDATETIME);
    END;

    LOCAL PROCEDURE ParseEmailBody(VAR JobQueueEntry: Record "Job Queue Entry") EmailBody: Text;
    VAR
        ErrorOccurredTxt: Label 'Ein Fehler ist aufgetreten:';
        PlaceHolderTxt: Label '<b>%1</b>: %2<br>', Locked = true;
    BEGIN
        EmailBody := STRSUBSTNO('<html><body><p><b>%1</b></p>', ErrorOccurredTxt);
        EmailBody += STRSUBSTNO('<h3>%1</h3> <p>', JobQueueEntry."Error Message");
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION(ID), JobQueueEntry.ID);
        IF JobQueueEntry."Run in User Session" THEN
            EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("User ID"), JobQueueEntry."User ID")
        ELSE
            EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("User ID"), USERID);
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("Last Ready State"), CURRENTDATETIME);
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("Object Type to Run"), JobQueueEntry."Object Type to Run");
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("Object ID to Run"), JobQueueEntry."Object ID to Run");
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION(Description), JobQueueEntry.Description);
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION(Status), JobQueueEntry.Status::Error);
        EmailBody += STRSUBSTNO(PlaceHolderTxt, JobQueueEntry.FIELDCAPTION("Job Queue Category Code"), JobQueueEntry."Job Queue Category Code");
        EmailBody += '</p></body></html>';
    END;


}