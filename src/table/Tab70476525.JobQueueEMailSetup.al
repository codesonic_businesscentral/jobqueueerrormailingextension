table 70476525 "Job Queue E-Mail Setup"
{
    Caption = 'Aufgabenwarteschlange E-Mail Einrichtung';
    fields
    {
        field(1; "Primary Key"; Code[10])
        {
            Caption = 'Primärschlussel';
        }
        field(2; "Send Mail From Name"; Text[80])
        {

            Caption = 'E-Mail senden von: Name';
            NotBlank = true;
        }
        field(3; "Send Mail From Address"; Text[80])
        {
            ExtendedDatatype = EMail;
            Caption = 'E-Mail senden von: Addresse';
            NotBlank = true;
        }
    }
    keys
    {
        key(key1; "Primary Key")
        {
            Clustered = true;
        }
    }
}
