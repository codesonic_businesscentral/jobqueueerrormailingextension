table 70476526 "JQ Mailing Group"
{
    Caption = 'Aufgabenwarteschlangeposten Verteiler';
    DataCaptionFields = "Code", "Description";
    LookupPageId = "Job Queue Mailing Groups";
    DrillDownPageId = "Job Queue Mailing Groups";
    fields
    {
        field(1; "Code"; Code[10])
        {
            Caption = 'Code';
            NotBlank = true;
        }
        field(2; "Description"; Text[50])
        {
            Caption = 'Beschreibung';
        }
        field(10; "Inactive"; Boolean)
        {
            Caption = 'Inaktiv';
        }
    }

    keys
    {
        key(key1; "Code")
        {
            Clustered = true;
        }

    }
    trigger OnDelete()
    begin
        DeleteMailingGroupMembers("Code");
    end;

    local procedure DeleteMailingGroupMembers("CodeIn": Code[10])
    var
        JQMailingGroupMember: record "JQ Mailing Group Member";
    begin
        JQMailingGroupMember.LOCKTABLE;
        JQMailingGroupMember.SETRANGE("JQ Mailing Group Code", CodeIn);
        JQMailingGroupMember.DELETEALL(TRUE);
    end;
}