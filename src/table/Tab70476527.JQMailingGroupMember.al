table 70476527 "JQ Mailing Group Member"
{
    Caption = 'Aufgabenwarteschlangeposten Verteiler Mitglied';
    DataCaptionFields = "JQ Mailing Group Code", "E-Mail";
    LookupPageId = "JQ Mailing Group Members";
    DrillDownPageId = "JQ Mailing Group Members";
    fields
    {
        field(1; "JQ Mailing Group Code"; Code[10])
        {
            Caption = 'Aufgabenarteschlangeposten Verteiler Mitgliedcode';
            NotBlank = true;
            TableRelation = "JQ Mailing Group";
        }
        field(2; "E-Mail"; Text[80])
        {
            Caption = 'E-Mail';
            ExtendedDatatype = EMail;
            NotBlank = true;
            trigger OnValidate()
            begin
                if "E-Mail" <> '' then
                    ValidateEmail("E-Mail");
            end;
        }

    }
    keys
    {
        key(key1; "JQ Mailing Group Code", "E-Mail")
        {
            Clustered = true;
        }
    }
    local procedure ValidateEmail(EmailIn: Text[80])
    var
        EmailNotValidErr: Label 'Die angegebene E-Mail-Addresse %1 ist ungültig.';
        Pattern: Text;
        DotNetRegex: Codeunit DotNet_Regex;
    begin
        Pattern := '^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,4}$';
        DotNetRegex.Regex(Pattern);
        if not DotNetRegex.IsMatch(EmailIn) then
            Error(EmailNotValidErr, EmailIn);
    end;
}