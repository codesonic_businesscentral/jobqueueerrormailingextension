page 70476527 "JQ Mailing Group Members"
{
    Caption = 'Aufgabenwarteschlangeposten Verteiler Mitglieder';
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = "JQ Mailing Group Member";
    DataCaptionFields = "JQ Mailing Group Code", "E-Mail";

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("JQ Mailing Group Code"; "JQ Mailing Group Code")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("E-Mail"; "E-Mail")
                {
                    ApplicationArea = all;
                    ExtendedDatatype = EMail;
                }

            }
        }
        area(Factboxes)
        {
            systempart(Notes; Notes) { }
            systempart(RecordLinks; Links) { }

        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction();
                begin

                end;
            }
        }
    }
}