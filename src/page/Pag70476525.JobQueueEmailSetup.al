page 70476525 "Job Queue E-mail Setup"
{

    PageType = Card;
    SourceTable = "Job Queue E-Mail Setup";
    Caption = 'Aufgabenwarteschlange E-Mail Einrichtung';
    InsertAllowed = false;
    DeleteAllowed = false;
    UsageCategory = Administration;
    ApplicationArea = All;

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'Allgemein';
                field("Send Mail From Name"; "Send Mail From Name")
                {
                    ApplicationArea = All;
                }
                field("Send Mail From Address"; "Send Mail From Address")
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    trigger OnOpenPage()
    begin
        InsertIfNotExists();
    end;

    local procedure InsertIfNotExists()
    begin
        if not get then begin
            Init();
            Insert();
        end;
    end;
}
