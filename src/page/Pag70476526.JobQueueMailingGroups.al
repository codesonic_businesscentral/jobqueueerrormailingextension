page 70476526 "Job Queue Mailing Groups"
{
    Caption = 'Aufgabenwarteschlangeposten Verteiler';
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = "JQ Mailing Group";
    DataCaptionFields = "code", "Description";

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(Code; Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = all;
                }
                field(Inactive; Inactive)
                {
                    ApplicationArea = all;
                }

            }
        }
        area(Factboxes)
        {
            systempart(Notes; Notes)
            {

            }
            systempart(RecordLinks; Links)
            {

            }

        }
    }

    actions
    {
        area(Processing)
        {
            action(Members)
            {
                Caption = 'Verteiler Mitglieder';
                ApplicationArea = All;
                RunObject = page "JQ Mailing Group Members";
                Promoted = true;
                Image = ListPage;
                RunPageLink = "JQ Mailing Group Code" = field("Code");

                trigger OnAction();
                begin

                end;
            }
        }
    }
}